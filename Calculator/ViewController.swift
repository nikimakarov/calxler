//
//  ViewController.swift
//  Calculator
//
//  Created by Nikita Makarov on 24/09/2017.
//  Copyright © 2017 Nikita Makarov. All rights reserved.
//

import UIKit

class ViewController: UIViewController
{
    @IBOutlet weak var display: UILabel!
    @IBOutlet weak var roundResetButton: RoundButton!
    @IBOutlet var roundButtons: [RoundButton]!
    @IBOutlet weak var operations: UILabel!
    
    var userIsInTheMiddleofTyping = false
    
    private func updateUI() {
        operations.text = (brain.descriptionGetter.isEmpty ? " " : brain.getDescription())
        displayValue = brain.result!
    }

    @IBAction func touchDigit(_ sender: UIButton) {
        let digit = sender.currentTitle!
        roundResetButton.setTitle("C", for: .normal)
        if userIsInTheMiddleofTyping {
            var textCurrentlyInDisplay = display.text!
            if (textCurrentlyInDisplay == "0" && digit != ".") {
                textCurrentlyInDisplay = ""
            }
            if !(textCurrentlyInDisplay.index(of: ".") != nil && digit == ".") {
                display.text = textCurrentlyInDisplay + digit
            }
        }
        else {
            display.text = (digit != ".") ? digit : "0.";
            userIsInTheMiddleofTyping = true
        }
    }
    
    @IBAction func touchAC(_ sender: UIButton) {
        displayValue = 0
        brain.setOperand(0)
        brain.resetParameters()
        brain.lastOperationInfo?.addInfo(symbol: "", type: "")
        operations.text = brain.descriptionGetter
        brain.variableValues.removeValue(forKey: "M")
        roundResetButton.setTitle("AC", for: .normal)
        userIsInTheMiddleofTyping = false
    }
    
    var savedProgram: CalculatorMain.PropertyList?
    
    @IBAction func save() {
        savedProgram = brain.program
    }
    
    @IBAction func restore() {
        if savedProgram != nil {
            brain.program = savedProgram!
            displayValue = brain.result!
        }
    }
    
    @IBAction func backspace(sender: UIButton) {
        guard userIsInTheMiddleofTyping == true else {
            brain.undo()
            updateUI()
            return
        }
        
        guard var number = display.text else {
            return
        }
        
        number.removeLast()
        if number.isEmpty {
            number = "0"
            userIsInTheMiddleofTyping = false
        }
        display.text = number
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        let rotation = UIDevice.current.orientation.isLandscape ? "landscape" : "portait";
        for button in roundButtons{
            button.changeRadius(format: rotation)
        }
    }
    
    var displayValue: Double{
        get{
            return Double(display.text!)!
        }
        set{
            if newValue.isFinite && newValue < pow(10,18) {
                if brain.isIntNumber(newValue) {
                    display.text = brain.formatDouble(newValue)
                }
                else{
                    display.text = String(Int(newValue))
                }
            }
            else{
                display.text = String("Error")
            }
        }
    }
    
    private var brain = CalculatorMain()
    
    @IBAction func performOperation(_ sender: UIButton) {
        if userIsInTheMiddleofTyping {
            brain.setOperand(displayValue)
            userIsInTheMiddleofTyping = false
        }
        if let mathematicalSymbol = sender.currentTitle {
            brain.performOperation(symbol: mathematicalSymbol)
        }
        updateUI()
    }
    
    @IBAction func setVariable() {
        brain.variableValues["M"] = displayValue
        if userIsInTheMiddleofTyping {
            userIsInTheMiddleofTyping = false
        } else {
            brain.undo()
        }
        brain.program = brain.program
        updateUI()
    }
    
    
    @IBAction func getVariable() {
        brain.setOperand(variable: "M")
        userIsInTheMiddleofTyping = false
        updateUI()
    }
    
    
}


