//
//  CalculatorMain.swift
//  Calculator
//
//  Created by Nikita Makarov on 08/10/2017.
//  Copyright © 2017 Nikita Makarov. All rights reserved.
//

import Foundation

func sind(degrees: Double) -> Double {
    return sin(degrees * Double.pi / 180.0)
}

func cosd(degrees: Double) -> Double {
    return cos(degrees * Double.pi / 180.0)
}

func tand(degrees: Double) -> Double {
    return tan(degrees * Double.pi / 180.0)
}

struct CalculatorMain {
    
    var variableValues = Dictionary<String,Double>()
    
    private var accumulator: Double? = 0.0
    private var internalProgram = [AnyObject]()
    
    private var isPartialResult: Bool {
        get {
            return pendingBinaryOperation != nil
        }
    }
    
    var descriptionGetter: String {
        get {
            if pendingBinaryOperation == nil {
                return description
            } else {
                return pendingBinaryOperation!.descriptionFunction(pendingBinaryOperation!.descriptionOperand,
                    pendingBinaryOperation!.descriptionOperand != description ? description : "")
            }
        }
    }
    
    private enum Priority: Int {
        case Min = 0, Max
    }
    
    private var description = "0" {
        didSet {
            if pendingBinaryOperation == nil {
                currentPrecedence = Priority.Max
            }
        }
    }
    
    private var currentPrecedence = Priority.Max
    
    var evenCall : Bool? = false
    var showLastNumber : Bool? = false
    
    mutating func resetParameters(){
        accumulator = 0.0
        pendingBinaryOperation = nil
        description = " "
        internalProgram.removeAll()
    }
    
    mutating func setOperand(_ operand : Double){
        accumulator = operand
        description = String(format:"%g", operand)
        internalProgram.append(operand as AnyObject)
    }
    
    mutating func setOperand(variable name: String) {
        variableValues[name] = variableValues[name] ?? 0.0
        accumulator = variableValues[name]!
        description = name
        internalProgram.append(name as AnyObject)
    }
    
    mutating func addDescription(_ additionalString : String){
        description = description + additionalString
    }
    
    func isIntNumber(_ value: Double) -> Bool {
        return Double(value) - Double(Int(value)) != 0
    }
    
    func makeInt(_ value: Double) -> String {
        if value.isFinite && value < pow(10,18){
            return isIntNumber(value) ? formatDouble(value) : String(Int(value))
        }
        else{
            return String("Error")
        }
    }
    
    func formatDouble(_ value: Double) -> String {
        let nf = NumberFormatter()
        nf.numberStyle = NumberFormatter.Style.decimal
        nf.maximumFractionDigits = 6
        return nf.string(from: NSNumber(value: value))!
    }
    
    mutating func addUnaryDescription(_ operation : String, _ value: String){
        if !(evenCall!) && lastOperationInfo!.type != ""{
            description = operation + "(" + description + ")"
        }
        else{
            description = description + operation + "(" + value + ")"
        }
    }
    
    private enum Operation {
        case constant(Double)
        case unaryOperation((Double) -> Double, (String) -> String)
        case binaryOperation((Double, Double) -> Double, (String, String) -> String, Priority)
        case equals
    }
    
    private var operations: Dictionary<String, Operation> =
        [
            "e" : Operation.constant(M_E),
            "π" : Operation.constant(.pi),
            "rand" : Operation.constant(drand48()),
            "√" : Operation.unaryOperation(sqrt,  {"√(\($0))"}),
            "cos" : Operation.unaryOperation(cosd, {"cos(\($0))"}),
            "sin" : Operation.unaryOperation(sind, {"sin(\($0))"}),
            "tan" : Operation.unaryOperation(tand, {"tan(\($0))"}),
            "ln" : Operation.unaryOperation(log, {"ln(\($0))"}),
            "log₂x" : Operation.unaryOperation(log2, {"log₂(\($0))"}),
            "x²" : Operation.unaryOperation({pow($0,2)}, {"(\($0))²"}),
            "±" : Operation.unaryOperation({-$0}, {"-(\($0))"}),
            "%" : Operation.unaryOperation({$0 / 100}, {"(\($0))%"}),
            "÷" : Operation.binaryOperation({$0 / $1}, {"\($0) ÷ \($1)"}, Priority.Max),
            "×" : Operation.binaryOperation({$0 * $1}, {"\($0) × \($1)"}, Priority.Max),
            "+" : Operation.binaryOperation({$0 + $1}, {"\($0) + \($1)"}, Priority.Min),
            "-" : Operation.binaryOperation({$0 - $1}, {"\($0) - \($1)"}, Priority.Min),
            "=" : Operation.equals
    ]
    
    struct LastOperationInfo {
        var symbol : String?
        var type : String?
        
        mutating func addInfo(symbol : String, type : String){
            self.symbol = symbol
            self.type = type
        }
    }
    
    var lastOperationInfo : LastOperationInfo? = LastOperationInfo(symbol: "", type: "")

    mutating func performOperation(symbol: String) {
        internalProgram.append(symbol as AnyObject)
        if let operation = operations[symbol] {
            switch operation {
            case .constant(let value):
                accumulator = value
                description = symbol
            case .unaryOperation(let resultFunction, let descriptionFunction):
                accumulator = resultFunction(accumulator!)
                description = descriptionFunction(description)
            case .binaryOperation(let resultFunction, let descriptionFunction, let precedence):
                performPendingBinaryOperation()
                if currentPrecedence.rawValue < precedence.rawValue {
                    description = "(\(description))"
                }
                currentPrecedence = precedence
                pendingBinaryOperation = PendingBinaryOperation(function: resultFunction, firstOperand: accumulator!,
                                                     descriptionFunction: descriptionFunction, descriptionOperand: description)
            case .equals:
                performPendingBinaryOperation()
            }
        }
    }

    mutating func undo() {
        if !internalProgram.isEmpty {
            internalProgram.removeLast()
            program = internalProgram as CalculatorMain.PropertyList
        } else {
            resetParameters()
            description = ""
        }
    }
    
    private mutating func performPendingBinaryOperation(){
        if  pendingBinaryOperation != nil {
            accumulator = pendingBinaryOperation!.function(pendingBinaryOperation!.firstOperand, accumulator!)
            description = pendingBinaryOperation!.descriptionFunction(pendingBinaryOperation!.descriptionOperand, description)
            pendingBinaryOperation = nil
        }
    }
    
    private var pendingBinaryOperation: PendingBinaryOperation?
    
    private struct PendingBinaryOperation {
        let function: (Double, Double) -> Double
        let firstOperand: Double
        let descriptionFunction: (String, String) -> String
        let descriptionOperand: String
    }
    
    typealias PropertyList = AnyObject
    var program: PropertyList {
        get{
            return internalProgram as CalculatorMain.PropertyList
        }
        set{
            resetParameters()
            if let arrayOfOps = newValue as? [AnyObject] {
                for op in arrayOfOps {
                    if let operand = op as? Double {
                        setOperand(operand)
                    } else if let variableName = op as? String {
                        if variableValues[variableName] != nil {
                            setOperand(variable: variableName)
                        } else if let operation = op as? String {
                            performOperation(symbol:operation)
                        }
                    }
                }
            }
        }
    }
    
    var result : Double? {
        get {
            if (accumulator != nil){
                return accumulator!
            }
            else {
                return 0
            }
        }
    }
    
    func getDescription() -> String {
        let whitespace = ((description.hasSuffix(" ")) ? "" : " ")
        return isPartialResult ? (description + whitespace  + "...") : (description + whitespace  + "=")
    }
    
}
