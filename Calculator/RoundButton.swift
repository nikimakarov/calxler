//
//  RoundButton.swift
//  Calculator
//
//  Created by Nikita Makarov on 24/09/2017.
//  Copyright © 2017 Nikita Makarov. All rights reserved.
//

import UIKit

@IBDesignable
class RoundButton: UIButton {

    @IBInspectable var cornerRadius: CGFloat = 0{
        didSet {
            self.layer.cornerRadius = self.frame.size.height / 2
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var borderColor: UIColor = UIColor.clear {
        didSet {
            self.layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable var backColorOnSelection: UIColor = UIColor.clear
    var savedColorBeforeSelection: UIColor = UIColor.clear
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        savedColorBeforeSelection = self.backgroundColor!
        UIView.animate(withDuration: 0.07) {
            self.backgroundColor = self.backColorOnSelection
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        UIView.animate(withDuration: 0.07) {
            self.backgroundColor = self.savedColorBeforeSelection
        }
    }
    
    func changeRadius(format: String) {
        if (format == "landscape"){
            self.layer.cornerRadius = self.frame.size.height / 4.5
        }
        else{
            self.layer.cornerRadius = self.frame.size.height
        }
    }

}
